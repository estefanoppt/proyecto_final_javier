
// Este archivo implementa lo que son las 
// funcionalidades principales de
// la base de datos de este proyecto. El objetivo es que el 
// codigo de servidor de este proyecto haga interfas
// con este codigo para realizar las peticiones.

#include"base_de_datos.h"
#include"db_server_errors.h"

#include<stdio.h>
#include<sys/stat.h>
#include<stdlib.h>
#include<fcntl.h>
#include<string.h>
#include<unistd.h>
#include<errno.h>

#include"uthash.h"

#define MAX_N 16384

// Esta estructura de datos servira para representar
// los elementos del hash tal como lo explica
// la documentacion de uthash.h.
typedef struct ELEMENTOS_HASH_TDA {
	char* llave;
	size_t pos_cursor;
	UT_hash_handle hh;
} ELEMENTOS_HASH;

// Se usara esta estructura de datos para mantener
// todas las conexiones abiertas. 
typedef struct HASH_BD_TDA {
	char* dir_bd;
	ELEMENTOS_HASH* tabla_de_elementos;
	UT_hash_handle hh;
} HASH_BD;

// Esta variable estatica servira para mantener 
// en orden todas las bases de datos abiertas.
static HASH_BD* PRINCIPAL_HASH_BD = NULL;

// Estas representan los simbolos a utilizar
// ambos para el elemento eliminado y para
// tener una cabecera en cada archivo. 
// (Esta idea se la atribuyo a Estefano 
// Palacios, quien me dio unas pautas de como
// hacer esta parte).
const char* LAPIDA_ELIMINAR = "@T!";
const char* LAPIDA_CABECERA = "@INICIO!:@NA";

int CREAR_BD(const char* dir_bd){

	// Empezamos asumiendo que no hay error, si en
	// cualquier punto hay error, cambiamos el valor
	// de esta variable y retornamos -1. Este sera
	// el procedimiento en el resto de funciones
	// de este segmento de codigo.
	db_server_errno = 0;

	if (!dir_bd){
		db_server_errno = NULL_DB_DIR;
		return -1;
	}

	// Primero verificamos que la base de datos no este
	// ya cargada.
	HASH_BD* dbh = NULL;
	HASH_FIND( hh, PRINCIPAL_HASH_BD, dir_bd, strlen(dir_bd) + 1, dbh);

	// Si ya este cargada, retornamos error.
	if (dbh){
		db_server_errno = DB_ALREADY_LOADED;
		return -1;		
	}

	// Por siguiente verificamos que el archivo de la base
	// de datos ya exist o no. Si no existe, creamos el 
	// archivo y ponemos un simbolo de cabecera. 
	if (access(dir_bd, F_OK) == -1){
		int permiso_archivo = S_IRUSR | S_IWUSR  | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH;
		int banderas_abrir  = O_CREAT | O_WRONLY;

		umask(0);
		int fd_db = open(dir_bd, banderas_abrir, permiso_archivo);
		if (fd_db == -1){
			db_server_errno = CANNOT_CREATE_FILE;
			return -1;
		}
		
		write(fd_db, LAPIDA_CABECERA, strlen(LAPIDA_CABECERA));
		write(fd_db, "\n", 1);

		close(fd_db);
	}

	printf("Here be dangerous mermaids.\n");
	printf("dir_db = %s\n", dir_bd);


	return ABRIR_BD(dir_bd);
}


int ABRIR_BD(const char* dir_bd){

	db_server_errno = 0;

	if (!dir_bd){
		db_server_errno = NULL_DB_DIR;
		return -1;
	}

	// Primer paso es verificar si la base de datos ya esta 
	// cargada.
	HASH_BD* dbh = NULL;
	HASH_FIND( hh, PRINCIPAL_HASH_BD, dir_bd, strlen(dir_bd) + 1, dbh);

	if (dbh){
		db_server_errno = DB_ALREADY_LOADED;
		return -1;		
	}

	// Como siguiente paso, verificamos que la base de
	// datos existe abriendola. Utilizamos fopen() en
	// lugar del mas general open() de UNIX para poder
	// hacer uso de getline() abajo.
	FILE* archivo = fopen(dir_bd, "r");
	if (archivo == NULL){
		db_server_errno = DB_DOES_NOT_EXIST;
		return -1;
	}


	// Ahora que sabemos que la base de datos existe,
	// creamos una nueva tabla de hash e iteramos por 
	// el archivo para construir la tabla.

	ELEMENTOS_HASH* HASH_BD_ABIERTA = NULL;

	// pos_cursor ira guardando la posicion del cursor
	// durante el loop.
	size_t pos_cursor = 0;

	// leidos guardara la cantidad de caracteres leidos
	// en un getline().
	size_t leidos     = 0;

       	size_t n           = MAX_N;
	char* buffer_linea = (char*)malloc(sizeof(char)*MAX_N);
	memset(buffer_linea, 0, MAX_N);
	
	while ((leidos = getline(&buffer_linea, &n, archivo)) != -1){
		// Usamos strtok() para hacer parsig de cada linea.
		char* llave      = strtok(buffer_linea, ":");
		size_t llave_len = strlen(llave) + 1;

		char* valor      = strtok(NULL, "\n");

		ELEMENTOS_HASH* elemento = NULL;
		HASH_FIND( hh, HASH_BD_ABIERTA, llave, llave_len, elemento);

		if (elemento == NULL){
			elemento = (ELEMENTOS_HASH*) malloc(sizeof(ELEMENTOS_HASH));

			char* llave_copia = (char*) malloc(sizeof(char) * llave_len);
			strcpy(llave_copia, llave);
			elemento->llave    = llave_copia;

			HASH_ADD_KEYPTR( hh, HASH_BD_ABIERTA, elemento->llave, llave_len, elemento);
		}

		elemento->pos_cursor = pos_cursor;
		pos_cursor += leidos;

		// Si el simbolo de eliminado fue encontrado, sacar elemento
		// de la hash table.
		if (strcmp(valor, LAPIDA_ELIMINAR) == 0){
			HASH_DEL(HASH_BD_ABIERTA, elemento);
			free(elemento->llave);
			free(elemento);
		}

		memset(buffer_linea, 0, MAX_N);
	}
	// Recuperando recursos usados en parseo.
	free(buffer_linea);
	fclose(archivo);
	
	// We assign the newly-created hash to the database hash collection.
	size_t dir_bd_len = strlen(dir_bd) + 1;
	char* dir_bd_llave  = (char*) malloc(sizeof(char) * dir_bd_len);
	memset(dir_bd_llave, 0, dir_bd_len);

	strcpy(dir_bd_llave, dir_bd);

	dbh                     = (HASH_BD*) malloc(sizeof(HASH_BD));
	dbh->dir_bd             = dir_bd_llave;
	dbh->tabla_de_elementos = HASH_BD_ABIERTA;

	HASH_ADD_KEYPTR( hh, PRINCIPAL_HASH_BD, dbh->dir_bd, dir_bd_len, dbh);

	return 0;
}

int INGRESAR_VALOR(const char* dir_bd, const char* llave, const char* valor){

	db_server_errno = 0;

	// Revisamos que nos hayan enviado un directorio valido (no NULL).
	if (!dir_bd){
		db_server_errno = NULL_DB_DIR;
		return -1;
	}

	// Revisamos que la llave que nos enviaron no sea NULL.
	if (!llave){
		db_server_errno = NULL_DB_KEY;
		return -1;
	}

	// Lo mismo para el valor: que no sea NULL.
	if (!valor){
		db_server_errno = NULL_DB_VALUE;
		return -1;
	}

	// Primero verificamos que la base de datos este cargada, si
	// no lo esta retornamos -1 en error.
	HASH_BD* dbh = NULL;
	HASH_FIND( hh, PRINCIPAL_HASH_BD, dir_bd, strlen(dir_bd) + 1, dbh);

	if (!dbh){
		db_server_errno = DB_NOT_LOADED;
		return -1;	
	}
	
	// Abrimos el archivo correspondiente a la base de datos. 
	// Aqui si usamos el open() de UNIX en lugar de fopen() de 
	// C standard para poder usar lseek().
	int banderas = O_WRONLY;
	
	int fd_db = open(dir_bd, banderas, 0);
	if (fd_db < 0){
		db_server_errno = DB_DOES_NOT_EXIST;
		return -1;
	}

	// Como estamos insertando, simplemente movemos el
	// cursor al final del archivo. Como estamos reconstruyendo
	// el hash en cada conexion la estructura se va a mantener.
	off_t pos_cursor = lseek(fd_db, 0, SEEK_END);

	// Escribimos la nueva entrada al archivo.
	write(fd_db, llave, strlen(llave));
	write(fd_db, ":", 1);
	write(fd_db, valor, strlen(valor));
	write(fd_db, "\n", 1);

	// Cerramos el archivo.
	close(fd_db);

	// Asi mismo, agregamos el nuevo elemento a la tabla hash.
	ELEMENTOS_HASH* tabla_objetivo = dbh->tabla_de_elementos;
	ELEMENTOS_HASH* elemento         = NULL;

	size_t llave_len = strlen(llave) + 1;
	HASH_FIND( hh, tabla_objetivo, llave, llave_len, elemento);

	if (elemento == NULL){
		elemento = (ELEMENTOS_HASH*) malloc(sizeof(ELEMENTOS_HASH));

		char* nueva_llave  = (char*) malloc(sizeof(char) * llave_len);
		strcpy(nueva_llave, llave);

		elemento->llave = nueva_llave;

		HASH_ADD_KEYPTR( hh, tabla_objetivo, elemento->llave, llave_len, elemento);
	}

	elemento->pos_cursor = pos_cursor;
	
	return 0;
}

int OBTENER_VALOR(const char* dir_bd, const char* llave, char** valor_mem_ptr){

	db_server_errno = 0;

	if (!dir_bd){
		db_server_errno = NULL_DB_DIR;
		return -1;
	}

	if (!llave){
		db_server_errno = NULL_DB_KEY;
		return -1;
	}

	// De nuevo verificamos que la base de datos este cargada.
	HASH_BD* dbh = NULL;
	HASH_FIND( hh, PRINCIPAL_HASH_BD, dir_bd, strlen(dir_bd) + 1, dbh);

	if (!dbh){
		db_server_errno = DB_NOT_LOADED;
		return -1;		
	}

	// Verificamos que el elemento exista.
	ELEMENTOS_HASH* tabla_objetivo = dbh->tabla_de_elementos;
	ELEMENTOS_HASH* elemento_buscado = NULL;

	size_t llave_len = strlen(llave) + 1;
	HASH_FIND( hh, tabla_objetivo, llave, llave_len, elemento_buscado);

	if (!elemento_buscado){
		db_server_errno = KEY_DOES_NOT_EXIST;	
		return -1;
	}

	// Si lo encontramos, abrimos el archivo de base de datos.
	// Nuevamente usamos UNIX open() para poder usar lseek().
	int open_flags = O_RDONLY;
	int fd_db      = open(dir_bd, open_flags, 0);
	if (fd_db < 0){
		db_server_errno = DB_DOES_NOT_EXIST;
		return -1;
	}

	// Saltamos a la ubicacion correcta. 
	size_t pos_cursor = elemento_buscado->pos_cursor;
	lseek(fd_db, pos_cursor, SEEK_SET);
	
	// A continuacion sacamos el valor del archivo.
	int status = 0; // 0 es cargando llave, 1 de cargando valor

	char byte_leido = 0;
	char buffer_linea[MAX_N];
	memset(buffer_linea, 0, MAX_N);

	int i = 0;

	while (read(fd_db, &byte_leido, 1)){
		if (byte_leido == ':'){
			status = 1;
			continue;
		}

		if (byte_leido == '\n'){
			buffer_linea[i]     = '\0';
			char* valor_mem    = (char*)malloc(sizeof(char)*(i + 1));
			strcpy(valor_mem, buffer_linea);
		
			*valor_mem_ptr     = valor_mem;	
			return 0;
		}

		if (status == 0) continue;  
		
		if (status == 1) {
			// Hack: limiting valor's size to MAX_N
			if (i < MAX_N - 1){
				buffer_linea[i++] = byte_leido;
			}
		}
	}

	db_server_errno = KEY_HAS_NO_VALUE;
	return -1;
}


int ELIMINAR_VALOR(const char* dir_bd, const char* llave){

	db_server_errno = 0;

	if (!dir_bd){
		db_server_errno = NULL_DB_DIR;
		return -1;
	}

	if (!llave){
		db_server_errno = NULL_DB_KEY;
		return -1;
	}

	HASH_BD* dbh = NULL;

	// Verificamos que la base de datos ya este cargada.
	HASH_FIND( hh, PRINCIPAL_HASH_BD, dir_bd, strlen(dir_bd) + 1, dbh);

	if (!dbh){
		db_server_errno = DB_NOT_LOADED;
		return -1;		
	}

	// Revisamos que el elemento exista en la base de datos.
	ELEMENTOS_HASH* tabla_objetivo = dbh->tabla_de_elementos;
	ELEMENTOS_HASH* elemento_a_eliminar         = NULL;

	HASH_FIND( hh, tabla_objetivo, llave, strlen(llave) + 1, elemento_a_eliminar);

	if (elemento_a_eliminar == NULL){
		db_server_errno = KEY_DOES_NOT_EXIST;
		return -1;
	}

	// Si elemento existe, eliminarlo.
	HASH_DELETE( hh, tabla_objetivo, elemento_a_eliminar);

	// We also need to add a tombstone entry in the file.
	int open_flags = O_WRONLY;
	
	int fd_db = open(dir_bd, open_flags, 0);
	if (fd_db < 0){
		db_server_errno = DB_DOES_NOT_EXIST;
		return -1;
	}

	// We move the file's cursor to the end of the file.
	lseek(fd_db, 0, SEEK_END);

	// We forge the new entry and write it in the file.
	write(fd_db, llave, strlen(elemento_a_eliminar->llave));
	write(fd_db, ":", 1);
	write(fd_db, LAPIDA_ELIMINAR, strlen(LAPIDA_ELIMINAR));
	write(fd_db, "\n", 1);

	// We free the memory from deleted llave
	free(elemento_a_eliminar->llave);
	free(elemento_a_eliminar);

	close(fd_db);

	return 0;
}
// --------------------------------------------------------------------------------
int CERRAR_BD(const char* dir_bd){
	
	db_server_errno = 0;

	if (dir_bd == NULL){
		db_server_errno = NULL_DB_DIR;
		return -1;
	}

	// Revisamos que la base datos este cargada.
	HASH_BD* base_tabla = NULL;

	HASH_FIND( hh, PRINCIPAL_HASH_BD, dir_bd, strlen(dir_bd) + 1, base_tabla);

	if (!base_tabla){
		db_server_errno = DB_NOT_LOADED;
		return -1;		
	}

	// If it's loaded, we iterate over all items, cleaning as we go.
	ELEMENTOS_HASH* hash_objetivo = base_tabla->tabla_de_elementos;

	ELEMENTOS_HASH* it = hash_objetivo;
	while (it != NULL){
		ELEMENTOS_HASH* next = it->hh.next;

		HASH_DEL(hash_objetivo, it);
		free(it->llave);
		free(it);	

		it = next;
	}
	
	HASH_DEL(PRINCIPAL_HASH_BD, base_tabla);
	
	free(base_tabla->dir_bd);
	free(base_tabla);

	return 0;
}

// --------------------------------------------------------------------------------
int IMPRIMIR_TODOS(const char* dir_bd){

	HASH_BD* dbh = NULL;

	// First we check whether database is already loaded.
	HASH_FIND_STR( PRINCIPAL_HASH_BD, dir_bd, dbh );

	if (!dbh){
		db_server_errno = DB_NOT_LOADED;
		return -1;		
	}

	ELEMENTOS_HASH* queried_table = dbh->tabla_de_elementos;

	char* valor_mem_ptr = NULL;
	for (ELEMENTOS_HASH* it = queried_table->hh.next; it != NULL; it = it->hh.next){
		OBTENER_VALOR(dir_bd, it->llave, &valor_mem_ptr);
		printf("KEY: %s, OFFSET: %lu, VALUE: %s\n", 
				it->llave, it->pos_cursor, valor_mem_ptr);
	}

	return 0;

}












