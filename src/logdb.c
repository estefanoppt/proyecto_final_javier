#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<errno.h>

#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>

#include<arpa/inet.h>
#include<sys/socket.h>

#include"base_de_datos.h"
#include"db_server_errors.h"

#define MAX_N 1024

static int OBTENER_NOMBRE_BASE(int fd_cliente, const char* dir_bd, char** resultado_buffer);
static int OBTENER_STR(int fd_cliente, char** resultado_buffer);

char* CMD_CREAR_BASE(int fd_cliente, char* dir_completo_bd, const char* dir_bd){

	// Grabamos el directorio de la base anterir en caso de que abrir falle
	// y haya que revertir los cambios.
	char* dir_anterior = dir_completo_bd;
	char* dir_nuevo = NULL;

	printf("Cliente quiere crear base de datos %s.", dir_bd);
	int resultado = OBTENER_NOMBRE_BASE(fd_cliente, dir_bd, &dir_nuevo);
	if (!resultado) resultado = CREAR_BD(dir_nuevo);

	// Si fallo la creacion, revertir cambios.
	if (resultado == 0){
		dir_completo_bd = dir_nuevo;
		printf("La base %s se creo con con exito.\n", dir_nuevo);
	} else {
		printf("No se pudo crear base de datos %s\n", dir_nuevo);
	}
	
	if (resultado == 0 && dir_anterior != NULL && strcmp(dir_nuevo, dir_anterior) != 0){
		resultado = CERRAR_BD(dir_anterior);

		if (resultado == 0){
			free(dir_anterior);
		} else {
			printf("No se pudo cerrar base %s abierta\n", dir_anterior);
			free(dir_nuevo);
			dir_completo_bd = dir_anterior;
		}
	}

	// Retornamos resultado al cliente.
	int n_db_server_errno = htonl(db_server_errno);
	write(fd_cliente, &n_db_server_errno, sizeof(int));

	return dir_completo_bd;
}


char* CMD_ABRIR_BASE(int fd_cliente, char* dir_completo_bd, const char* dir_bd){

	// De nuevo, guardamos base anterior en caso que operacion falle.
	char* dir_anterior = dir_completo_bd;
	char* dir_nuevo = NULL;

	printf("Cliente quiere abrir base de datos %s\n", dir_bd);

	int resultado = OBTENER_NOMBRE_BASE(fd_cliente, dir_bd, &dir_nuevo);

	// Si no hubieron errores al obtener base,
	// entonces abrimos la base.
	if (resultado == 0) resultado = ABRIR_BD(dir_nuevo);
	
	// Si fallo apertura, revertir.
	if (resultado == 0){
		dir_completo_bd = dir_nuevo;
		printf("Se abrio base con exito!\n");
	} else {
		printf("Fallo abrir base de datos.\n");
	}

	// Cerramos base anteriormente abierta.
	if (resultado == 0 && dir_anterior != NULL && strcmp(dir_nuevo, dir_anterior) != 0){
		resultado = CERRAR_BD(dir_anterior);

		if (resultado == 0){
			free(dir_anterior);
		} else {
			printf("No se pudo cerrar anterior base de datos.\n");

			free(dir_nuevo);
			dir_completo_bd = dir_anterior;
		}
	}
	
	// Retornamos resultado al cliente.
	int n_db_server_errno = htonl(db_server_errno);
	write(fd_cliente, &n_db_server_errno, sizeof(int));

	return dir_completo_bd;
}


void CMD_INSERTAR_VALOR(int fd_cliente, const char* dir_completo_bd){

	// Necesitamos obtener la llave primero.
	char* buffer_llave = NULL;
	int resultado = OBTENER_STR(fd_cliente, &buffer_llave);

	printf("Se nos pide insertar un valor.\n");

	// De alli lo siguiente es obtener el valor.
	char* buffer_valor = NULL;
	if (resultado == 0) 
		resultado = OBTENER_STR(fd_cliente, &buffer_valor);

	// Si todo ha salido bien hasta ahora, poner valor en db.
	if (resultado == 0) 
		resultado = INGRESAR_VALOR(dir_completo_bd, buffer_llave, buffer_valor);

	if (resultado == 0){
		printf("Se insertoi el valor con exito.");
	} else {
		printf("No se inserto el valor con exito.");
	}

	// Liberamos recursos.
	free(buffer_llave);
	free(buffer_valor);

	// Retornamos el resultado a cliente.
	int n_db_server_errno = htonl(db_server_errno);
	write(fd_cliente, &n_db_server_errno, sizeof(int));

	return;
}

void CMD_OBTENER_VALOR(int fd_cliente, const char* dir_completo_bd){

	char* buffer_llave = NULL;
	int resultado;
	int gs_resultado;

	printf("Cliente quiere obtener un valor.\n");
       	gs_resultado = resultado = OBTENER_STR(fd_cliente, &buffer_llave);

	int gv_resultado;
	char* buffer_valor = NULL;
	if (resultado == 0)
		gv_resultado = resultado = OBTENER_VALOR(dir_completo_bd, buffer_llave, &buffer_valor);

	int n_db_server_errno = htonl(db_server_errno);
	write(fd_cliente, &n_db_server_errno, sizeof(int));

	uint32_t value_len;
	if (resultado == 0){
		value_len   = (uint32_t)(1 + strlen(buffer_valor));

		uint32_t n_value_len = htonl(value_len);
		ssize_t n_written    = write(fd_cliente, &n_value_len, sizeof(uint32_t));

		if (n_written < sizeof(uint32_t)){
			db_server_errno = SIZE_MESSAGE_FAILED;
			resultado = -1;
		}
	}

	if (resultado == 0){
		ssize_t n_written = write(fd_cliente, buffer_valor, value_len);

		if (n_written < value_len){
			db_server_errno = VALUE_MESSAGE_FAILED;
			resultado = -1;
		}
	}

	if (resultado == 0){
		printf("Fue exitoso el envio del elemento con llave (%s) y con valor (%s).\n", buffer_llave, buffer_valor);
	} else {
		printf("Fallo el envio del elemento con llave (%s) y con valor (%s).\n", buffer_llave, buffer_valor);
	}

	if (gs_resultado == 0) free(buffer_valor);
	if (gv_resultado == 0) free(buffer_llave);

	return;
}

static void CMD_ELIMINAR_VALOR(int fd_cliente, const char* dir_completo_bd){

	printf("El cliente desea eliminar un valor.\n");

	char* buffer_llave = NULL;
	int resultado       = OBTENER_STR(fd_cliente, &buffer_llave);
	
	if (resultado == 0) 
		resultado = ELIMINAR_VALOR(dir_completo_bd, buffer_llave);

	if (resultado == 0){
		db_server_errno = 0;
		printf("Fue exitosa la eliminacion del valor con llave (%s).\n", buffer_llave);
	} else {
		printf("Fallo la eliminacion del valor con llave (%s).\n", buffer_llave);
	}
	
	int n_db_server_errno = htonl(db_server_errno);
	write(fd_cliente, &n_db_server_errno, sizeof(int));

	free(buffer_llave);

	return;
}

static void CMD_CERRAR_BASE(int fd_cliente, char* dir_completo_bd){

	printf("El cliente quiere cerrar la base datos y terminar la conexion con el servidor.\n");

	int resultado = CERRAR_BD(dir_completo_bd);

	if (resultado == 0){
		printf("Fue exitoso el cierre de la base de datos %s.\n", dir_completo_bd);
	} else {
		printf("Fallo en cerrar la base de datos %s.\n", dir_completo_bd);
	}

	int n_db_server_errno = htonl(db_server_errno);
	write(fd_cliente, &n_db_server_errno, sizeof(int));

	free(dir_completo_bd);

	close(fd_cliente);
	printf("Fue exitosa la desconexion con el cliente.\n");
}

static int OBTENER_NOMBRE_BASE(int fd_cliente, const char* dir_bd, char** resultado_buffer){
	
	uint32_t db_name_len;
	ssize_t leidos = read(fd_cliente, &db_name_len, sizeof(uint32_t));
	db_name_len      = ntohl(db_name_len);

	if (leidos < sizeof(uint32_t)){
		db_server_errno = SIZE_MESSAGE_FAILED;
		return -1;
	}


	char* db_name_buffer = (char*)malloc(sizeof(char) * db_name_len);

	leidos = read(fd_cliente, db_name_buffer, db_name_len);
	if (leidos < db_name_len){
		db_server_errno = DB_DIR_RECEIVE_FAILED;
		free(db_name_buffer);
		return -1;
	}


	*resultado_buffer = (char*)malloc(sizeof(char) * (strlen(dir_bd) + db_name_len + 1));
 
	sprintf(*resultado_buffer, "%s/%s", dir_bd, db_name_buffer);

	free(db_name_buffer);

	return 0;
}

static int OBTENER_STR(int fd_cliente, char** resultado_buffer){


	uint32_t str_len;
	ssize_t leidos = read(fd_cliente, &str_len, sizeof(uint32_t));
	str_len          = ntohl(str_len);

	if (leidos < sizeof(uint32_t)){
		db_server_errno = SIZE_MESSAGE_FAILED;
		return -1;
	}

	
	*resultado_buffer = (char*) malloc(sizeof(char)*str_len);	
		
	leidos = read(fd_cliente, *resultado_buffer, str_len);
	if (leidos < str_len){
		db_server_errno = DB_DIR_RECEIVE_FAILED;
		free(resultado_buffer);
		return -1;
	}


	return 0;

}

int main(int argc, const char* argv[]){
	
	if (argc < 3){
		printf("Uso: %s dir-bd ip puerto\n", argv[0]);
		exit(1);
	}

	const char* dir_bd = argv[1];
	const char* ip = argv[2];
	int puerto = atoi(argv[3]);

	printf("Inicializando servidor.\n Creando el el servidor.\n");
	
	struct sockaddr_in dir_servidor;
	memset(&dir_servidor, 0, sizeof(dir_servidor));
	dir_servidor.sin_port = htons(puerto);
	dir_servidor.sin_addr.s_addr = inet_addr(ip);
	dir_servidor.sin_family = AF_INET;

	int socket_fd = socket(dir_servidor.sin_family, SOCK_STREAM, 0);
	if (!socket_fd){
		printf("Error llamando socket().\n");
		exit(0);
	}

	int bind_res = bind(socket_fd, (struct sockaddr*)&dir_servidor, sizeof(dir_servidor));
	if (bind_res != 0){
		printf("Error al llamar bind().\n");
		close(socket_fd);
		exit(0);
	}

	if (listen(socket_fd, 64)){
		const char* e_msg = strerror(errno);
		printf("Error al llamar a listen(): %s.\n", e_msg);
		close(socket_fd);
		exit(0);
	}


	while (1){
		printf("\nEsperando a un cliente cualquiera.\n");

		int fd_cliente = accept(socket_fd, NULL, NULL);
		printf("\nSe ha establecido una conexion con un cliente.\n");

		char* dir_completo_bd = NULL;
		while (1){

			int tamano_operacion; 
			int leidos = read(fd_cliente, &tamano_operacion, sizeof(uint32_t));
			tamano_operacion = ntohl(tamano_operacion);

			if (!leidos) {
				close(fd_cliente);
				break;
			}

			char buffer_operacion[MAX_N];
			memset(buffer_operacion, 0, MAX_N - 1);

			leidos = read(fd_cliente, buffer_operacion, tamano_operacion);

			if (strcmp(buffer_operacion, "OPEN_DB") == 0){
				dir_completo_bd = CMD_ABRIR_BASE(fd_cliente, dir_completo_bd, dir_bd);
			} else if (strcmp(buffer_operacion, "CREATE_DB") == 0){
				dir_completo_bd = CMD_CREAR_BASE(fd_cliente, dir_completo_bd, dir_bd);
			} else if(strcmp(buffer_operacion, "PUT_VAL") == 0){
				CMD_INSERTAR_VALOR(fd_cliente, dir_completo_bd);
			} else if (strcmp(buffer_operacion, "GET_VAL") == 0){
				CMD_OBTENER_VALOR(fd_cliente, dir_completo_bd);
			} else if(strcmp(buffer_operacion, "DELETE") == 0){
				CMD_ELIMINAR_VALOR(fd_cliente, dir_completo_bd);
			} else if(strcmp(buffer_operacion, "CLOSE_DB") == 0){
				CMD_CERRAR_BASE(fd_cliente, dir_completo_bd);
				break;
			}
		}
	}

	close(socket_fd);

	return 0;
}
