#include "db_client_errors.h"

// Client has its own error flags global variable.
int db_client_errno = 0;

const char* db_client_strerror(int db_client_errno){
	switch (db_client_errno){
		case CLIENT_SUCCESS:
			return "Exito del cliente.";
		case NO_CONNECTION:
			return "Conexion no ha sido establecida con el servidor.";
		case NO_OPEN_DB:
			return "No hay base de datos abierta por el momento.";
		case FAILED_TYPE_SEND:
			return "Fallo el envio del tipo de requisito.";
		case FAILED_SIZE_SEND:
			return "Fallo el envio del tamano del siguiente requisito.";
		case FAILED_ARG_SEND:
			return "Fallo el envio de uno de los argumentos del requisito.";
		case FAILED_RESULT_RECEIVE:
			return "Fallo el recibir el resultado de la operacion del servidor.";
		case FAILED_SIZE_RECEIVE:
			return "Fallo el recibir del tamano del valor requerido.";
		case FAILED_VALUE_RECEIVE:
			return "Fallo el recibir del valor requerido.";
		case DB_STILL_OPEN:
			return "Base de datos todavia esta abierta en servidor.";
		case SERVER_ERROR:
			return "Hubo un error del lado del servidor.";
	}

	return "Codigo de error invalido.";
}
