#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, const char* argv[]){

	FILE* archivo = fopen(argv[1], "r");

	if (archivo == NULL){
		fprintf(stderr, "error al abrir archivo.\n");
		exit(EXIT_FAILURE);
	}

	//char* ptr_linea = NULL;
	size_t n = 100;
	char* ptr_linea = NULL; //(char*) malloc(sizeof(char)*n);
	//memset(ptr_linea, 0, n);

	while (getline(&ptr_linea, &n, archivo) > 1){
		char* key = strtok(ptr_linea, ":");
		char* value = strtok(NULL, "\n");

		printf("%s => %s\n", key, value);
		free(ptr_linea);
	}

	return 0;
}


