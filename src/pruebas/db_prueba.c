#include "base_de_datos.h"
#include "db_server_errors.h"

#include <stdio.h>

int main(int argc, const char* argv[]){

	const char* db_dir = argv[1];
	CREAR_DB(db_dir);

	INGRESAR_VALOR(db_dir, "hey", "you");
	//printf("%d: %s\n", res, db_server_strerror(db_server_errno));
	INGRESAR_VALOR(db_dir, "another", "displeased");
	
	char* value = NULL;
	OBTENER_VALOR(db_dir, "hey", &value);
	printf("%s\n", value);

	ELIMINAR_VALOR(db_dir, "hey");

	INGRESAR_VALOR(db_dir, "lacasadesalso", "estamuyloca");

	IMPRIMIR_TODOS(db_dir);

	CERRAR_DB(db_dir);

	return 0;
}
