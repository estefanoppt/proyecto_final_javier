// -------------------------------------------
// ESTE ES EL INCLUDE MAS IMPORTANTE DE TODOS
// EN ESTE ARCHIVO FUENTE.
#include"logdb.h"
// -------------------------------------------

#include<arpa/inet.h>
#include<unistd.h>
#include<stdio.h>
#include<sys/stat.h>
#include<sys/socket.h>
#include<fcntl.h>
#include<string.h>
#include<stdlib.h>
#include<errno.h>

#include"uthash.h"

#include"db_client_errors.h"
#include"db_server_errors.h"

#define MAX_SLEEP 64

conexionlogdb* conectar_db(const char *ip, int puerto){

	struct sockaddr_in dir_clinte;
	memset(&dir_clinte, 0, sizeof(dir_clinte));

	dir_clinte.sin_port        = htons(puerto);
	dir_clinte.sin_family      = AF_INET;
	dir_clinte.sin_addr.s_addr = inet_addr(ip);

	int socket_fd;
	int num_sec = 1;
	while (1) {
		if ((socket_fd = socket(dir_clinte.sin_family, SOCK_STREAM, 0)) < 0){
			printf("Hubo un error al crear el descriptor de puerto.\n");
			exit(EXIT_FAILURE);
		}

		int connect_result = connect(
					socket_fd, 
					(struct sockaddr*)&dir_clinte,
					sizeof(dir_clinte));

		if (connect_result == 0){ break; } 

		printf("Hubo un error al intentar conectar el cliente con el servidor.\n");
		if (num_sec > MAX_SLEEP){
			printf("No hay mas intentos restantes. Cancelando conextion.\n");
			exit(EXIT_FAILURE);
		} 			
		
		printf("Intentando nuevamente conectarse con el servidor.\n");
		close(socket_fd);

		sleep(num_sec);
		num_sec <<= 1;
	}

	conexionlogdb* logdb = (conexionlogdb*)malloc(sizeof(conexionlogdb));

	logdb->ip = (char*)malloc(sizeof(char)*(strlen(ip) + 1));
	strcpy(logdb->ip, ip);

	logdb->puerto    = puerto;
	logdb->id_sesion = 1;
	logdb->nombredb  = NULL;
	logdb->sockfd  = socket_fd;

	return logdb;
}	

static int _send_request_type(conexionlogdb* connection, const char* connection_type){
	
	uint32_t type_len  = (uint32_t)(1 + strlen(connection_type));
	uint32_t ntype_len = htonl(type_len);

	ssize_t n_written = write(
			connection->sockfd,
			&ntype_len,
			sizeof(uint32_t));

	if (n_written < sizeof(uint32_t)){
		db_client_errno = FAILED_SIZE_SEND;
		return -1;
	}
			
	n_written = write(
		connection->sockfd, 
		connection_type, 
		type_len);

	if (n_written < type_len){
		db_client_errno = FAILED_TYPE_SEND;
		return -1;
	}	

	return 0;
}

static int _send_database_name(conexionlogdb* connection, const char* db_name){
	
	uint32_t db_len   = (uint32_t)(1 + strlen(db_name));

	uint32_t n_db_len = htonl(db_len);
	ssize_t n_written = write(connection->sockfd, &n_db_len, sizeof(uint32_t));

	if (n_written < sizeof(uint32_t)){
		db_client_errno = FAILED_SIZE_SEND;
		return -1;
	}

	n_written = write(connection->sockfd, db_name, db_len);

	if (n_written < db_len){
		db_client_errno = FAILED_ARG_SEND;
		return -1;
	}

	return 0;
}

static int _send_string(conexionlogdb* connection, const char* str){
	
	uint32_t str_len   = (uint32_t)(1 + strlen(str));

	uint32_t n_str_len = htonl(str_len);
	ssize_t n_written  = write(connection->sockfd, &n_str_len, sizeof(uint32_t));

	if (n_written < sizeof(uint32_t)){
		db_client_errno = FAILED_SIZE_SEND;
		return -1;
	}

	n_written = write(connection->sockfd, str, str_len);

	if (n_written < str_len){
		db_client_errno = FAILED_ARG_SEND;
		return -1;
	}

	return 0;
}

static int _get_server_result(conexionlogdb* connection){
	int server_result;
	ssize_t num_read = read(connection->sockfd, &server_result, sizeof(int));
	server_result    = ntohl(server_result);

	if (num_read < sizeof(server_result)){
		db_client_errno = FAILED_RESULT_RECEIVE;
		return -1;
	}

	if (server_result != 0){
		db_client_errno = SERVER_ERROR;
		db_server_errno = server_result;
		return -1;
	}

	return 0;
}

int crear_db(conexionlogdb *conexion, char *nombre_db){
	if (conexion == NULL || conexion->id_sesion < 0){
		db_client_errno = NO_CONNECTION;
		return -1;		
	}

	printf("CREAR DB: Enviando solicitud para creacion de base de datos.\n");

	int result = _send_request_type(conexion, "CREATE_DB");
	if (result < 0) return result;

	result = _send_database_name(conexion, nombre_db);
	if (result < 0) return result;

	result = _get_server_result(conexion);
	if (result < 0) return result;

	if (conexion->nombredb != NULL)
		free(conexion->nombredb);

	conexion->nombredb = (char*)malloc(sizeof(char)*strlen(nombre_db));
	strcpy(conexion->nombredb, nombre_db);

	return 0;
}


int abrir_db(conexionlogdb *conexion, char *nombre_db){

	if (conexion == NULL || conexion->id_sesion < 0){
		db_client_errno = NO_CONNECTION;
		return -1;		
	}

	printf("ABRIR DB: enviando solicitud para abrir db.\n");

	int result = _send_request_type(conexion, "OPEN_DB");
	if (result < 0) return result;

	result = _send_database_name(conexion, nombre_db);
	if (result < 0) return result;

	result = _get_server_result(conexion);
	if (result < 0) return result;

	if (conexion->nombredb != NULL)
		free(conexion->nombredb);

	conexion->nombredb = (char*)malloc(sizeof(char)*strlen(nombre_db));
	strcpy(conexion->nombredb, nombre_db);
	
	return 0;
}

int put_val(conexionlogdb *conexion, char* clave, char* valor){

	if (conexion == NULL || conexion->id_sesion < 0){
		db_client_errno = NO_CONNECTION;
		return -1;		
	}

	if (conexion->nombredb == NULL){
		db_client_errno = NO_OPEN_DB;
		return -1;
	}

	printf("INSERTAR VALOR: enviando solicitud para insertar un valor.\n");

	int result = _send_request_type(conexion, "PUT_VAL");
	if (result < 0) return result;

	result = _send_string(conexion, clave);
	if (result < 0) return result;

	result = _send_string(conexion, valor);
	if (result < 0) return result;

	return _get_server_result(conexion);
}

char* get_val(conexionlogdb* conexion, char* clave){

	if (conexion == NULL || conexion->id_sesion < 0){
		db_client_errno = NO_CONNECTION;
		return NULL;		
	}

	if (conexion->nombredb == NULL){
		db_client_errno = NO_OPEN_DB;
		return NULL;
	}

	printf("OBTENER VALOR: enviando solicitud para extraer un valor de la base de datos.\n");

	int result = _send_request_type(conexion, "GET_VAL");
	if (result < 0) return NULL;

	result = _send_string(conexion, clave);
	if (result < 0) return NULL;

	result = _get_server_result(conexion);
	if (result < 0) return NULL;

	uint32_t value_len;
	ssize_t num_read = read(conexion->sockfd, &value_len, sizeof(uint32_t));
	value_len        = ntohl(value_len);

	if (num_read < sizeof(uint32_t)){
		db_client_errno = FAILED_SIZE_RECEIVE;
		return NULL;
	}

	char* value_str = (char*)malloc(sizeof(char)*value_len);
	num_read = read(conexion->sockfd, value_str, value_len);

	if (num_read < value_len){
		db_client_errno = FAILED_RESULT_RECEIVE;
		free(value_str);
		return NULL;
	}

	return value_str;
}

int eliminar(conexionlogdb *conexion, char *clave){

	if (conexion == NULL || conexion->id_sesion < 0){
		db_client_errno = NO_CONNECTION;
		return -1;		
	}

	if (conexion->nombredb == NULL){
		db_client_errno = NO_OPEN_DB;
		return -1;
	}

	printf("ELIMINAR: enviando solicitud para eliminar una entrada.\n");

	int result = _send_request_type(conexion, "DELETE");
	if (result < 0) return -1;

	result = _send_string(conexion, clave);
	if (result < 0) return -1;
	
	return _get_server_result(conexion);
}

void cerrar_db(conexionlogdb *conexion){
	db_client_errno = 0;
	
	if (conexion == NULL || conexion->id_sesion < 0){
		db_client_errno = NO_CONNECTION;
		return;		
	}
	
	printf("CERRAR DB: enviando solicitud para cerrar base de datos.\n");
	if (conexion->nombredb != NULL){

		int result = _send_request_type(conexion, "CLOSE_DB");
		if (result < 0) return;

		result = _send_database_name(conexion, conexion->nombredb);
		if (result < 0) return;
		
		result = _get_server_result(conexion);
		if (result < 0) return;

		free(conexion->nombredb);
		conexion->nombredb = NULL;
	}

	_send_request_type(conexion, "DISCONNECT");
	close(conexion->sockfd);

	free(conexion->ip);
	free(conexion);
	
	return;
}


