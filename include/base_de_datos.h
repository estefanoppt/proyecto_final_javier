#ifndef DATABASE_H
#define DATABASE_H

int ABRIR_BD(const char* db_dir);

int CREAR_BD(const char* db_dir);

int INGRESAR_VALOR(const char* db_dir, const char* key, const char* value);

int OBTENER_VALOR(const char* db_dir, const char* key, char** value_mem_ptr);

int ELIMINAR_VALOR(const char* db_dir, const char* key);

int CERRAR_BD(const char* db_dir);

int IMPRIMIR_TODOS(const char* db_dir);

#endif
